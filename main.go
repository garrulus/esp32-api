package main

import (
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	MQTT "github.com/eclipse/paho.mqtt.golang"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"io/ioutil"
	"net/http"
	"strconv"
)

const (
	ENDPOINT                 = ""
	CLIENT_ID                = ""
	PATH_TO_CERTIFICATE      = "secrets/device.pem.crt"
	PATH_TO_PRIVATE_KEY      = "secrets/device.pem.key"
	PATH_TO_AMAZON_ROOT_CA_1 = "secrets/AmazonRootCA1.pem"
	TOPIC                    = "esp32/sub"
	QOS                      = 0
)

type Api struct {
	e          *echo.Echo
	mqqtClient MQTT.Client
}

type Mode struct {
	Mode string `json:"mode"`
}

type ColorDTO struct {
	ColorHEX string `json:"color"`
}

type Color struct {
	Red   int `json:"red"`
	Green int `json:"green"`
	Blue  int `json:"blue"`
}

func main() {

	api := Api{
		initEcho(),
		initMQTTClient(),
	}

	api.initRoutes()
	api.e.Logger.Fatal(api.e.Start(":8010"))
}

func (api Api) initRoutes() {
	api.e.POST("/mode", func(c echo.Context) (err error) {
		mode := new(Mode)
		if err = c.Bind(mode); err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}

		jsonBytes, err := json.Marshal(mode)
		if err != nil {
			fmt.Println("Error marshaling struct to JSON:", err)
			return
		}

		token := api.mqqtClient.Publish(
			TOPIC,
			QOS,
			false,
			jsonBytes,
		)

		token.Wait()
		if token.Error() != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}

		return c.JSON(http.StatusOK, nil)
	})

	api.e.POST("/color", func(c echo.Context) (err error) {
		colorDTO := new(ColorDTO)
		if err = c.Bind(colorDTO); err != nil {
			return echo.NewHTTPError(http.StatusBadRequest, err.Error())
		}

		r, g, b, err := hexToRGB(colorDTO.ColorHEX)
		color := Color{
			Red:   r,
			Green: g,
			Blue:  b,
		}

		jsonBytes, err := json.Marshal(color)
		if err != nil {
			fmt.Println("Error marshaling struct to JSON:", err)
			return
		}

		token := api.mqqtClient.Publish(
			TOPIC,
			QOS,
			false,
			jsonBytes,
		)

		token.Wait()
		if token.Error() != nil {
			return echo.NewHTTPError(http.StatusInternalServerError, err.Error())
		}

		return c.JSON(http.StatusOK, nil)
	})
}

func initEcho() *echo.Echo {
	e := echo.New()

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORS())

	return e
}

func initMQTTClient() MQTT.Client {
	opts := createMQTTOpts()

	client := MQTT.NewClient(opts)
	if token := client.Connect(); token.Wait() && token.Error() != nil {
		panic(token.Error())
	}

	return client
}

func createMQTTOpts() *MQTT.ClientOptions {
	certPool := x509.NewCertPool()
	pemCerts, err := ioutil.ReadFile(PATH_TO_AMAZON_ROOT_CA_1)
	if err == nil {
		certPool.AppendCertsFromPEM(pemCerts)
	}

	cert, err := tls.LoadX509KeyPair(PATH_TO_CERTIFICATE, PATH_TO_PRIVATE_KEY)
	if err != nil {
		panic(err)
	}

	tlsConfig := &tls.Config{
		Certificates: []tls.Certificate{cert},
		RootCAs:      certPool,
	}

	opts := MQTT.NewClientOptions()
	opts.AddBroker(fmt.Sprintf("ssl://%s:8883", ENDPOINT))
	opts.SetClientID(CLIENT_ID)
	opts.SetTLSConfig(tlsConfig)
	return opts
}

func hexToRGB(hex string) (int, int, int, error) {

	// Parse the hex color string
	if len(hex) == 7 && hex[0] == '#' {
		if r, err := strconv.ParseInt(hex[1:3], 16, 0); err != nil {
			return 0, 0, 0, err
		} else if g, err := strconv.ParseInt(hex[3:5], 16, 0); err != nil {
			return 0, 0, 0, err
		} else if b, err := strconv.ParseInt(hex[5:], 16, 0); err != nil {
			return 0, 0, 0, err
		} else {
			return int(r), int(g), int(b), nil
		}
	}

	return 0, 0, 0, fmt.Errorf("Invalid hex color string: %s", hex)
}
